import moment from 'moment'

export const get_window_size = window_width => {
  if (window_width > 1904) return 'xl'
  if (window_width > 1264) return 'lg'
  if (window_width > 960) return 'md'
  if (window_width > 600) return 'sm'
  return 'xs'
}

export const convert_datetime = (string, input_format, output_format) => {
  const datetime = moment(string, input_format)
  return datetime.format(output_format)
}

export const is_empty_object = obj => {
  return Object.keys(obj).length === 0
}


export const string_to_datetime = (string, format) => {
  return moment(string, format)
}

export const get_now = () => {
  return moment()
}

export default {
  install: (Vue, opts) => {
    Vue.prototype.$utils = {
      get_window_size,
      is_empty_object,
      string_to_datetime,
      get_now,
      convert_datetime,
    }
  }
}
