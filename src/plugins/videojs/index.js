import VuePlayer from './player.vue'

export default function (Vue, options) {
  if (options) {
    VuePlayer.props.default_options.default = () => config.options
  }
  Vue.component(VuePlayer.name, VuePlayer)
}


