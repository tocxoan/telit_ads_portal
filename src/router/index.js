import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '*',
      redirect: '/e404',
      component: () => import('../views/errors/e404')
    },
    {
      path: '/e401',
      component: () => import('../views/errors/e401')
    },
    {
      path: '/',
      name: 'Layout',
      component: () => import('../views/layout'),
      redirect: '/campaign',
      children: [
        {
          path: 'campaign',
          name: 'Campaign',
          component: () => import('../views/campaign'),
        },
        {
          path: 'auth',
          name: 'Auth',
          component: () => import('../views/auth'),
        }
      ]
    }
  ]
})
