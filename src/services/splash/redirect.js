export default async (login_url, data) => {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", login_url, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onload = function () {
        location.href = data.dst;
    }
    xhr.onerror = function () {
        location.href = data.dst;
    };
    xhr.send("username="+data.username+"&password="+data.password+"&dst="+data.dst);
    return 'ok';
}
