import get from './get'
import login from './login'
import redirectLogin from './redirect'
import sendOtp from './send-otp'
import verifyCode from './verify-code'

export default {
    get,
    login,
    redirectLogin,
    sendOtp,
    verifyCode
}
