/**
 * Created by shin on 22/06/2017.
 */
import Vuex from 'vuex'
import Vue from 'vue'

import Device from './device'
import Common from './common'
import Campaign from './campaign'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Device,
    Campaign,
    Common
  },
  strict: process.env.NODE_ENV !== 'production'
})
