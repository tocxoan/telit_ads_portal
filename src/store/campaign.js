export default {
  namespaced: true,
  state: {
    seen: false,
    splash: {
      redirect_url: null
    },
    survey: []
  },
  mutations: {
    seen (state) {
      state.seen = true
    },
    fetch_splash (state, data) {
      state.splash = data
    },
    set_survey (state, data) {
      state.survey = data
    },
  },
  actions: {},
  getters: {
    splash: (state) => state.splash,
    survey: (state) => state.survey
  }
}
