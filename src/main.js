import Vue from 'vue'
import {mapGetters} from 'vuex'
import VueVideoPlayer from 'vue-video-player'

import App from './app'
import router from './router'
import store from './store'
import axios from 'axios';
import VueAxios from 'vue-axios';

import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';

Raven.config('https://aeabb0658b574485aaa5b2c3af09bc0f@sentry.paas.vn/109')
    .addPlugin(RavenVue, Vue)
    .install();

import './plugins/vuetify'
import video_player from './plugins/videojs'
import utils from './plugins/utils'

/** CONFIG VUE AXIOS **/
Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_API;
Vue.axios.defaults.headers['Content-Type'] = 'application/json'
Vue.axios.defaults.headers['Accept'] = 'application/json'
Vue.axios.defaults.withCredentials = true;


Vue.config.productionTip = false

Vue.use(utils)
Vue.use(video_player)
Vue.use(VueVideoPlayer, /* {
  options: global default options,
  events: global videojs events
} */)

Vue.mixin({
    computed: {
        ...mapGetters('Common', ['common_data']),
        ...mapGetters('Campaign', ['splash', 'survey'])
    }
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
