export default {
  namespaced: true,
  state: {
    data: {
      no_image_url: '/assets/no_image.png',
      max_content_length: 15 * 1024 * 1024,
      re_validators: {
        email: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
        phone: /^[0-9]{9,15}$/
      },
      datetime_format: 'DD/MM/YYYY'
    },
    constants_loaded: null
  },
  mutations: {
    loading_constants_success: (state, data) => {
      Object.assign(state.data, data)
      state.constants_loaded = true
    },
    constants_loaded: (state, value) => {
      state.constants_loaded = value
    }
  },
  actions: {},
  getters: {
    common_data: state => state.data
  }
}
