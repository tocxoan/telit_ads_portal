export default {
  namespaced: true,
  state: {
    size: 'sm',
    width: null,
    height: null
  },
  mutations: {
    screen_resized (state, {width, height, size}) {
      state.width = width
      state.height = height
      state.size = size
    }
  },
  actions: {},
  getters: {}
}
