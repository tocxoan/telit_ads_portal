import Vue from 'vue'

export default async (params) => {
    let url_params = ''
    Object.keys(params).forEach((key) => {
        url_params+= `${key}=${params[key]}&`
    })
    let payload = await Vue.axios.get(`/splash?${url_params}`)
    return payload.data
}
