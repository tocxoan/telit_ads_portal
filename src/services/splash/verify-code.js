import Vue from 'vue'

export default async (data) => {
    let payload = await Vue.axios.post(`/public/otp/verify`, data)
    return payload.data
}
