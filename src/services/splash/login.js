import Vue from 'vue'

export default async (data) => {
    let payload = await Vue.axios.post(`/splash`, data)
    return payload.data
}
